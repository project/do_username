#!/usr/bin/env bash

DIR_NAME=${PWD##*/}
NAME=${DIR_NAME//_/-}

DRUPAL_VER=${1:-drupal}

ddev stop
ddev delete -O
rm -rf .ddev/ web/ vendor/

ddev config --project-name=$NAME-$DRUPAL_VER --project-type=$DRUPAL_VER --docroot=web --create-docroot --php-version=8.3 --database=mariadb:11.4
ddev get ddev/ddev-drupal-contrib
ddev start
ddev poser

cp web/sites/example.settings.local.php web/sites/default/settings.local.php
echo "if (file_exists(\$app_root . '/' . \$site_path . '/settings.local.php')) {
  include \$app_root . '/' . \$site_path . '/settings.local.php';
}" >> web/sites/default/settings.php

ddev symlink-project
