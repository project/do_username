<?php

namespace Drupal\do_username;

use Drupal\Core\Cache\CacheBackendInterface;
use Hussainweb\DrupalApi\Client;
use Hussainweb\DrupalApi\Entity\User;
use Hussainweb\DrupalApi\Request\Collection\UserCollectionRequest;

/**
 * User information retriever service.
 *
 * This class is responsible for retrieving users information
 * from Drupal.org.
 */
class DOUserInfoRetriever {

  /**
   * Guzzle http client.
   *
   * @var \Hussainweb\DrupalApi\Client
   */
  protected $client;

  /**
   * Cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * User information service constructor.
   *
   * @param \Hussainweb\DrupalApi\Client $client
   *   Injected client service.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   Injected cache service.
   */
  public function __construct(Client $client, CacheBackendInterface $cache) {
    $this->client = $client;
    $this->cache = $cache;
  }

  /**
   * Get user information from a d.o username.
   *
   * @param string $doUsername
   *   The drupal.org username.
   *
   * @return ?\Hussainweb\DrupalApi\Entity\User
   *   User object with drupal.org user information.
   */
  public function getUserInformation(string $doUsername): ?User {
    $cid = 'do_username:user:' . $doUsername;

    // Check if the data exist. If not, retrieve, save, and return.
    $cachedData = $this->cache->get($cid);
    if ($cachedData) {
      return $cachedData->data;
    }
    $request = new UserCollectionRequest(["name" => $doUsername]);
    $userData = $this->client->getEntity($request);
    $userInfo = $userData->count() == 1 ? $userData->current() : NULL;
    $this->cache->set($cid, $userInfo);
    return $userInfo;
  }

  /**
   * Checks if given username is a valid d.o username.
   *
   * @param string $doUsername
   *   The drupal.org username.
   *
   * @return bool
   *   TRUE if username is valid, FALSE otherwise.
   */
  public function isValidUser(string $doUsername): bool {
    return $this->getUserInformation($doUsername)->__isset('uid');
  }

}
