<?php

namespace Drupal\do_username;

use Drupal\Core\TypedData\DataDefinitionInterface;
use Drupal\Core\TypedData\TypedData;
use Drupal\Core\TypedData\TypedDataInterface;

/**
 * Drupal Computed Fields Class .
 *
 *  This class uses the drupal user retriever service
 *  to get related fields from cache.
 */
class DOComputedFields extends TypedData {

  /**
   * The user service.
   *
   * @var \Drupal\do_username\DOUserInfoRetriever
   */
  protected $doUserService;

  /**
   * {@inheritdoc}
   */
  public function __construct(DataDefinitionInterface $definition, $name = NULL, TypedDataInterface $parent = NULL) {
    parent::__construct($definition, $name, $parent);
    $this->doUserService = \Drupal::service('do_username.user_service');
  }

  /**
   * The data value.
   *
   * @var mixed
   */
  protected $value;

  /**
   * Implements \Drupal\Core\TypedData\TypedDataInterface::getValue().
   */
  public function getValue($langcode = NULL) {
    $item = $this->getParent();
    $userName = $item->value;
    $userInformation = $this->doUserService->getUserInformation($userName);
    if (!$userInformation) {
      return NULL;
    }
    $field_type = $this->name;

    // Get the information from cache depending on the field type.
    switch ($field_type) {
      case 'bio':
        return $userInformation->field_bio->value ?? '';

      case 'contributions':
        return $userInformation->field_contributed;

      case 'country':
        return $userInformation->field_country;

      case 'created':
        return date('c', $userInformation->created);

      case 'first_name':
        return $userInformation->field_first_name;

      case 'irc_nick':
        return $userInformation->field_irc_nick;

      case 'languages':
        return $userInformation->field_languages;

      case 'last_name':
        return $userInformation->field_last_name;

      case 'url':
        return $userInformation->url;

      case 'websites':
        $websites = [];
        foreach ($userInformation->field_websites as $item) {
          $websites[] = $item->url;
        }
        return $websites;

      default:
        return 'N/A';
    }
  }

}
