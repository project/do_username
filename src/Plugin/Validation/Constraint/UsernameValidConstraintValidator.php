<?php

namespace Drupal\do_username\Plugin\Validation\Constraint;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\do_username\DOUserInfoRetriever;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the Username Valid constraint.
 */
class UsernameValidConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * The DO User information retriever service.
   *
   * @var \Drupal\do_username\DOUserInfoRetriever
   */
  protected $doUserService;

  /**
   * ConfigEntityUpdater constructor.
   *
   * @param \Drupal\do_username\DOUserInfoRetriever $do_user_service
   *   The DO User information retriever service.
   */
  public function __construct(DOUserInfoRetriever $do_user_service) {
    $this->doUserService = $do_user_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {

    return new static(
      $container->get('do_username.user_service')
    );

  }

  /**
   * {@inheritdoc}
   */
  public function validate($item, Constraint $constraint) {
    /** @var \Drupal\do_username\Plugin\Field\FieldType\DrupalOrgUsernameItem $item */
    /** @var \Drupal\do_username\Plugin\Validation\Constraint\UsernameValidConstraint $constraint */
    $name = $item->getValue()['value'];

    if ($item->validateUsername() && !$this->doUserService->isValidUser($name)) {
      $this->context->addViolation($constraint->invalidUserMessage);
    }

  }

}
