<?php

namespace Drupal\do_username\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Validation constraint for UserName items to ensure the format is correct.
 *
 * @Constraint(
 *   id = "DoUsernameFormat",
 *   label = @Translation("Valid UserName format.", context = "Validation"),
 * )
 */
class UsernameFormatConstraint extends Constraint {

  /**
   * Violation message when the username begins with a space.
   *
   * @var string
   */
  public $spaceBeginMessage = 'The username cannot begin with a space.';

  /**
   * Violation message when the username ends with a space.
   *
   * @var string
   */
  public $spaceEndMessage = 'The username cannot end with a space.';

  /**
   * Violation message when the username contains multiple spaces in a row.
   *
   * @var string
   */
  public $multipleSpacesMessage = 'The username cannot contain multiple spaces in a row.';

  /**
   * Violation message when the username contains an illegal character.
   *
   * @var string
   */
  public $illegalMessage = 'The username contains an illegal character.';

  /**
   * Violation message when the username exceeds its maximum character limit.
   *
   * @var string
   */
  public $tooLongMessage = 'The username %name is too long: it must be %max characters or less.';

}
