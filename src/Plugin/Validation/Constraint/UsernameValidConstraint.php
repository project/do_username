<?php

namespace Drupal\do_username\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Provides an Username Valid constraint.
 *
 * @Constraint(
 *   id = "UsernameValid",
 *   label = @Translation("Username Valid", context = "Validation"),
 * )
 */
class UsernameValidConstraint extends Constraint {

  /**
   * Violation message when username was not found on drupal.org.
   *
   * @var string
   */
  public $invalidUserMessage = 'The username was not found on drupal.org.';

}
