<?php

namespace Drupal\do_username\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'do_username_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "do_username_default",
 *   label = @Translation("Drupal.org username (link)"),
 *   field_types = {
 *     "do_username"
 *   }
 * )
 */
class DrupalOrgUsernameFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'rel' => '',
      'target' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['rel'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add rel="nofollow" to links'),
      '#return_value' => 'nofollow',
      '#default_value' => $this->getSetting('rel'),
    ];
    $elements['target'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Open link in new window'),
      '#return_value' => '_blank',
      '#default_value' => $this->getSetting('target'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $settings = $this->getSettings();

    if (!empty($settings['rel'])) {
      $summary[] = $this->t('Add rel="@rel"', ['@rel' => $settings['rel']]);
    }
    if (!empty($settings['target'])) {
      $summary[] = $this->t('Open link in new window');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = $this->viewValue($item);
    }

    return $elements;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return array
   *   The textual output generated.
   */
  protected function viewValue(FieldItemInterface $item) {
    $settings = $this->getSettings();

    $attributes = [];

    if (!empty($settings['rel'])) {
      $attributes['rel'] = $settings['rel'];
    }
    if (!empty($settings['target'])) {
      $attributes['target'] = $settings['target'];
    }

    $username = $item->value;

    // Try to get the URL from the API first. But we may not always be able to
    // get it. This could be due to an invalid username or many other reasons.
    // In that case, we just generate a dummy URL assuming the best.
    $url = $item->getProperties(TRUE)['url']->getValue();
    if (!$url) {
      $url = 'https://www.drupal.org/u/' . str_replace(" ", "-", strtolower($username));
    }

    return [
      '#type' => 'link',
      '#title' => $username,
      '#url' => Url::fromUri($url, [
        'attributes' => $attributes,
      ]),
    ];
  }

}
