<?php

namespace Drupal\do_username\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\user\UserInterface;

/**
 * Plugin implementation of the 'do_username_field_type' field type.
 *
 * @FieldType(
 *   id = "do_username",
 *   label = @Translation("Drupal.org username"),
 *   description = @Translation("This field stores a drupal.org Username."),
 *   default_widget = "do_username_text",
 *   default_formatter = "do_username_default",
 *   constraints = {"DoUsernameFormat" = {}, "UsernameValid" = {}}
 * )
 */
class DrupalOrgUsernameItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'max_length' => UserInterface::USERNAME_MAX_LENGTH,
      'do_validate' => TRUE,
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Drupal.org username'))
      ->setSetting('case_sensitive', $field_definition->getSetting('case_sensitive'))
      ->setRequired(TRUE);
    $properties['bio'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Biography'))
      ->setComputed(TRUE)
      ->setInternal(FALSE)
      ->setClass('\Drupal\do_username\DOComputedFields');
    $properties['contributions'] = DataDefinition::create('list')
      ->setLabel(new TranslatableMarkup('Contributions'))
      ->setComputed(TRUE)
      ->setInternal(FALSE)
      ->setClass('\Drupal\do_username\DOComputedFields');
    $properties['country'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Country'))
      ->setComputed(TRUE)
      ->setInternal(FALSE)
      ->setClass('\Drupal\do_username\DOComputedFields');
    $properties['created'] = DataDefinition::create('timestamp')
      ->setLabel(new TranslatableMarkup('Created on'))
      ->setComputed(TRUE)
      ->setInternal(FALSE)
      ->setClass('\Drupal\do_username\DOComputedFields');
    $properties['first_name'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('First Name'))
      ->setComputed(TRUE)
      ->setInternal(FALSE)
      ->setClass('\Drupal\do_username\DOComputedFields');
    $properties['irc_nick'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('IRC Nick'))
      ->setComputed(TRUE)
      ->setInternal(FALSE)
      ->setClass('\Drupal\do_username\DOComputedFields');
    $properties['languages'] = DataDefinition::create('list')
      ->setLabel(new TranslatableMarkup('Langages'))
      ->setComputed(TRUE)
      ->setInternal(FALSE)
      ->setClass('\Drupal\do_username\DOComputedFields');
    $properties['last_name'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Last name'))
      ->setComputed(TRUE)
      ->setInternal(FALSE)
      ->setClass('\Drupal\do_username\DOComputedFields');
    $properties['url'] = DataDefinition::create('uri')
      ->setLabel(new TranslatableMarkup('Url'))
      ->setComputed(TRUE)
      ->setInternal(FALSE)
      ->setClass('\Drupal\do_username\DOComputedFields');
    $properties['websites'] = DataDefinition::create('list')
      ->setLabel(new TranslatableMarkup('Websites'))
      ->setComputed(TRUE)
      ->setInternal(FALSE)
      ->setClass('\Drupal\do_username\DOComputedFields');
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = [
      'columns' => [
        'value' => [
          'type' => 'varchar',
          'length' => $field_definition->getSetting('max_length'),
        ],
      ],
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $random = new Random();
    $values['value'] = $random->word(mt_rand(1, $field_definition->getSetting('max_length')));
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $elements = [];

    $elements['max_length'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum length'),
      '#default_value' => $this->getSetting('max_length'),
      '#required' => TRUE,
      '#description' => $this->t('The maximum length of the field in characters.'),
      '#min' => 1,
      '#disabled' => $has_data,
    ];

    $elements['do_validate'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable username validation'),
      '#description' => $this->t('Enable this field to validate whether the given username exists in Drupal.org website.'),
      '#default_value' => $this->getSetting('do_validate'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * Check for username validation is enabled.
   *
   * @return bool
   *   TRUE if username validation is enabled.
   */
  public function validateUsername() {
    return $this->getSetting('do_validate');
  }

}
