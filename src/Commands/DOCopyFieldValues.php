<?php

namespace Drupal\do_username\Commands;

use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\EntityTypeManager;
use Drush\Commands\DrushCommands;

/**
 * A drush command file.
 *
 * @package Drupal\do_username\Commands
 */
class DOCopyFieldValues extends DrushCommands {

  /**
   * Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Entity Field Manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * Source field.
   *
   * @var string sourceField
   */
  protected $sourceField;

  /**
   * Destination field.
   *
   * @var string destinationField
   */
  protected $destinationField;

  /**
   * Constructs a new UpdateVideosStatsController object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   Entity type service.
   * @param \Drupal\Core\Entity\EntityFieldManager $entityFieldManager
   *   Entity type service.
   */
  public function __construct(EntityTypeManager $entityTypeManager, EntityFieldManager $entityFieldManager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->entityFieldManager = $entityFieldManager;
  }

  /**
   * Drush command that displays the given text.
   *
   * @param string $entity_type
   *   Entity type.
   * @param string $bundle
   *   Entity bundle.
   * @param string $destination_field
   *   Field to copy values to.
   * @param string $source_field
   *   Field to get values from.
   *
   * @command do_username:copy
   * @aliases du-copy
   * @usage do_username:copy entity_type bundle field_to field_from
   */
  public function copyFieldValue($entity_type, $bundle, $destination_field, $source_field) {
    $this->destinationField = $destination_field;
    $this->sourceField = $source_field;
    $fieldDefinitions = $this->entityFieldManager->getFieldDefinitions($entity_type, $bundle);
    if (!$this->isDestinationValid($fieldDefinitions)) {
      $this->output()->writeln('Invalid destination field. Please make sure the field exists and its type is text(formateed)');
      return;
    }
    if (!$this->isSourceValid($fieldDefinitions)) {
      $this->output()->writeln('Invalid source field. Please make sure the field exists and its type is text(formatted)');
      return;
    }
    $entityIds = $this->entityTypeManager->getStorage($entity_type)->getQuery()
      ->accessCheck(FALSE)
      ->condition('type', $bundle)
      ->condition('status', 1)
      ->execute();
    // @todo Chunk the result for processing with batch.
    $entities = $this->entityTypeManager->getStorage($entity_type)->loadMultiple($entityIds);
    $this->logger->info('Starting process for  @entities, and all after it.', ['@entities' => count($entities)]);
    foreach ($entities as $entity) {
      $sourceData = $entity->get($source_field)->first()->getValue();
      $destinationData = $entity->get($destination_field)->first()->getValue();
      if (!isset($destinationData)) {
        $entity->set($destination_field, $sourceData)->save();
      }
    }
    $this->logger->info('Field values copied successfully!');
  }

  /**
   * Check Destination field validity.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface[] $fieldDefinitions
   *   List of field definitions for the bundle.
   */
  protected function isDestinationValid($fieldDefinitions): bool {
    return isset($fieldDefinitions[$this->destinationField]) && $fieldDefinitions[$this->destinationField]->getType() == 'text';
  }

  /**
   * Check Source field validity.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface[] $fieldDefinitions
   *   List of field definitions for the bundle.
   */
  protected function isSourceValid($fieldDefinitions): bool {
    return isset($fieldDefinitions[$this->sourceField]) && $fieldDefinitions[$this->sourceField]->getType() == 'text';
  }

}
