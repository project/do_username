# Drupal.org Username

The Drupal.org username module provides a simple field to accept drupal.org
usernames. This behaves as a simple link field but also provides computed fields
which provides user data from drupal.org.

This is useful for Drupal community sites (such as DrupalCamp websites) which
typically accept drupal.org usernames. Such sites will be able to cleanly
accept usernames (with validation) and also display additional data available
from the [drupal.org API](https://www.drupal.org/drupalorg/docs/apis/rest-and-other-apis).

Right now, the module only provides this data via the
`do_username.user_service` but eventually, this data can also be shown in a
view.

Follow the development of this module at [the project page](https://www.drupal.org/project/do_username).

## Installation

Install as you would any Drupal module with composer.

```bash
$ composer require 'drupal/do_username:^2.0'
```

See [the documentation](https://www.drupal.org/docs/develop/using-composer/using-composer-to-install-drupal-and-manage-dependencies)
for more details.

## Configuration

When enabled, a field is added to user entities. You can enable the field on
the form display and entity display, if required. You can also add this field
type to other entity types as well similar to other field types.
