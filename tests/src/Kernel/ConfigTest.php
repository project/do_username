<?php

namespace Drupal\Tests\do_username\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests that the Drupal.org Username module config installs correctly.
 *
 * @group do_username
 */
class ConfigTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['do_username', 'field', 'user'];

  /**
   * Test if config installs properly.
   */
  public function testConfig() {
    $this->installEntitySchema('user');
    $this->installConfig(['do_username']);
  }

}
