<?php

namespace Drupal\Tests\custom\Kernel;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;

/**
 * Tests the 'do_username' formatter's functionality.
 *
 * @group do_username
 */
class DrupalOrgUsernameFormatterTest extends EntityKernelTestBase {

  /**
   * The entity type used in this test.
   *
   * @var string
   */
  protected $entityType = 'entity_test';

  /**
   * The bundle used in this test.
   *
   * @var string
   */
  protected $bundle = 'entity_test';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['do_username'];

  /**
   * Field type used to test.
   *
   * @var string
   */
  public $fieldType = 'do_username';

  /**
   * Machine name of field being tested.
   *
   * @var string
   */
  public $fieldName = 'field_under_test';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    FieldStorageConfig::create([
      'field_name' => 'field_under_test',
      'entity_type' => $this->entityType,
      'type' => $this->fieldType,
      'settings' => [],
    ])->save();
    FieldConfig::create([
      'entity_type' => $this->entityType,
      'bundle' => $this->bundle,
      'field_name' => $this->fieldName,
      'label' => 'Field label',
    ])->save();
  }

  /**
   * Given field data, create a test entity and return the target.
   */
  protected function createAndGetFieldRenderArray(array $field_data): array {
    // Create the entity to be referenced.
    /** @var \Drupal\Core\Entity\FieldableEntityInterface */
    $entity = $this->container->get('entity_type.manager')
      ->getStorage($this->entityType)
      ->create(['name' => $this->randomMachineName()]);

    $entity->field_under_test = $field_data;
    $entity->save();

    return $entity->get($this->fieldName)
      ->view(['type' => $this->fieldType]);
  }

  /**
   * Tests the formatter's output with a date in the past.
   *
   * @param string $username
   *   A drupal.org username.
   * @param string $url
   *   The expected URL.
   *
   * @dataProvider providerUsernameToUrl
   */
  public function testFormatterWithDateInThePast(string $username, string $url): void {
    $result = $this->createAndGetFieldRenderArray(['value' => $username]);
    $this->assertEquals($url, $result[0]['#url']->toString());
  }

  /**
   * Data provider for testing FieldFormatter.
   *
   * @return array
   *   Test data with username and the expected URL.
   */
  public function providerUsernameToUrl(): array {
    return [
      'invalidWithFallback' => ['John36', 'https://www.drupal.org/u/john36'],
      'invalidWithSpacesFallback' => ['John 36', 'https://www.drupal.org/u/john-36'],
      'actualUser' => ['Rajab Natshah', 'https://www.drupal.org/u/rajab-natshah'],
      'actualUserWithDots' => ['Mohammed J. Razem', 'https://www.drupal.org/u/mohammed-j-razem'],
    ];
  }

}
