<?php

namespace Drupal\Tests\do_username\Unit\Plugin\Validation\Constraint;

use Drupal\do_username\Plugin\Field\FieldType\DrupalOrgUsernameItem;
use Drupal\do_username\Plugin\Validation\Constraint\UsernameFormatConstraint;
use Drupal\do_username\Plugin\Validation\Constraint\UsernameFormatConstraintValidator;
use Drupal\Tests\UnitTestCase;
use Prophecy\Argument;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @coversDefaultClass \Drupal\do_username\Plugin\Validation\Constraint\UsernameFormatConstraintValidator
 * @group do_username
 */
class UsernameFormatConstraintValidatorTest extends UnitTestCase {

  /**
   * @covers ::validate
   *
   * @dataProvider providerTestValidate
   */
  public function testValidate($item, $expected_violation) {
    $constraint = new UsernameFormatConstraint();

    // If a violation is expected, then the context's addViolation method will
    // be called, otherwise it should not be called.
    $context = $this->prophesize(ExecutionContextInterface::class);

    if ($expected_violation) {
      $context->addViolation(Argument::any())->shouldBeCalledTimes(1);
    }
    else {
      $context->addViolation()->shouldNotBeCalled();
    }

    $validator = new UsernameFormatConstraintValidator();
    $validator->initialize($context->reveal());
    $validator->validate($item, $constraint);
  }

  /**
   * Data provider for ::testValidate().
   */
  public function providerTestValidate() {
    $cases = [];

    $item = $this->prophesize(DrupalOrgUsernameItem::class);
    $item->getValue()->willReturn(['value' => 'hussainweb']);
    $cases['Valid Username'] = [$item->reveal(), FALSE];

    $item = $this->prophesize(DrupalOrgUsernameItem::class);
    $item->getValue()->willReturn(['value' => ' hussainweb']);
    $cases['Username beginning with space'] = [$item->reveal(), TRUE];

    $item = $this->prophesize(DrupalOrgUsernameItem::class);
    $item->getValue()->willReturn(['value' => 'hussainweb ']);
    $cases['Username ending with space'] = [$item->reveal(), TRUE];

    $item = $this->prophesize(DrupalOrgUsernameItem::class);
    $item->getValue()->willReturn(['value' => 'hussain  web']);
    $cases['Username with multiple spaces'] = [$item->reveal(), TRUE];

    $item = $this->prophesize(DrupalOrgUsernameItem::class);
    $item->getValue()->willReturn(['value' => 'hussain[web]']);
    $cases['Username with illegal characters'] = [$item->reveal(), TRUE];

    return $cases;
  }

}
